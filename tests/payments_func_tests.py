from decimal import Decimal

from rest_framework.test import APITestCase
from django.contrib.auth.models import User

from accounts.models import Account, Currency
from payments.models import Payment


class AccountFunctionalApiTestCase(APITestCase):
    """ Tests for account module. """

    def setUp(self):
        self.user = User.objects.create_user(username='test', password='12345')
        self.client.force_authenticate(user=self.user)

    def test_payment_retrieve(self):
        currency = Currency.objects.create(name='Test Currency', code='TST')
        account = Account.objects.create(owner=self.user, name='Test', currency=currency)
        payment = Payment.objects.create(
            account=account,
            value=10,
            direction=Payment.DIRECTION_DICT[0][1]
            )

        assert Payment.objects.get(pk=account.pk).pk == payment.pk

        response = self.client.get('/v1/payments/%s/' % payment.pk)

        assert response.status_code == 200
        assert response.data['account']['name'] == account.name
        assert Decimal(response.data['value']) == payment.value
        assert response.data['direction'] == payment.direction

    def test_payment_good_post(self):
        currency = Currency.objects.create(name='Test Currency', code='TST')
        account = Account.objects.create(owner=self.user, name='Test', currency=currency)
        account.register_incoming(Decimal('10'))

        user2 = User.objects.create_user(username='test2', password='12345')
        account2 = Account.objects.create(owner=user2, name='Test2', currency=currency)
        assert account2.balance == 0

        self.client.post('/v1/payments/', {
            'amount': 10,
            'from_account': account.pk,
            'to_account': account2.pk,
        })

        account2 = Account.objects.get(pk=account2.pk)
        assert account2.balance == 10

    def test_payment_garbage_post(self):
        currency = Currency.objects.create(name='Test Currency', code='TST')
        account = Account.objects.create(owner=self.user, name='Test', currency=currency)
        assert Account.objects.get(pk=account.pk).pk == account.pk

        response = self.client.post('/v1/payments/', {
            'amount': 0,
        })

        assert response.status_code == 400

    def test_payments_retrieve(self):
        currency = Currency.objects.create(name='Test Currency', code='TST')

        account1 = Account.objects.create(owner=self.user, name='Test1', currency=currency)
        account1.register_incoming(Decimal('10'))

        user2 = User.objects.create_user(username='test2', password='12345')
        account2 = Account.objects.create(owner=user2, name='Test2', currency=currency)
        account2.register_incoming(Decimal('10'))
        account2.register_outgoing(Decimal('-9'))

        response = self.client.get('/v1/payments/')
        assert response.data[0]['account']['owner']['id'] == self.user.id
        assert len(response.data) == 1

    def tearDown(self):
        pass
