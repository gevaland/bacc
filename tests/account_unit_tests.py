import unittest
from decimal import Decimal
from time import time

from django.contrib.auth.models import User

from accounts import models, exceptions


class AccountTestCase(unittest.TestCase):
    """ Tests for account module. """

    def setUp(self):
        currency = models.Currency.objects.create(name='Test Currency', code='TST')
        user = User.objects.create_user(username=str(time()), password='12345')
        self.account = models.Account(owner=user, name='Test', currency=currency)
        self.account.save()

    def test_payment_create(self):
        self.account.register_incoming(Decimal('10'))
        assert self.account.balance == Decimal('10')

    def test_payment_two_transactions(self):
        self.account.register_incoming(Decimal('10'))
        self.account.register_outgoing(Decimal('-9'))
        assert self.account.balance == Decimal('1')

    def test_payment_lock_error(self):
        self.account.register_incoming(Decimal('10'))
        locked = models.Locked(self.account)
        with locked:
            try:
                self.account.register_outgoing(Decimal('-9'))
            except exceptions.AccountLocked:
                assert self.account.balance == Decimal('10')
                return
            assert not 'No lock error.'

    def test_payment_predefined_lock(self):
        self.account.register_incoming(Decimal('10'))
        locked = models.Locked(self.account)
        with locked:
            self.account.register_outgoing(Decimal('-9'), locked)
            assert self.account.balance == Decimal('1')

    def test_lock_passing(self):
        self.account.register_incoming(Decimal('10'))
        with models.Locked(self.account) as lock:
            try:
                self.account.register_outgoing(
                    Decimal('-9'),
                    current_lock=lock)
            except exceptions.AccountLocked:
                assert not 'Test should share lock.'
            assert self.account.balance == Decimal('1')

    def tearDown(self):
        pass
