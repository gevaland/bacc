from decimal import Decimal

from rest_framework.test import APITestCase
from django.contrib.auth.models import User

from accounts.models import Account, Currency


class AccountFunctionalApiTestCase(APITestCase):
    """ Tests for account module. """

    def setUp(self):
        self.user = User.objects.create_user(username='test', password='123456')
        self.currency = Currency.objects.create(name='Test Currency', code='TST')
        self.client.force_authenticate(user=self.user)

    def test_account_retrieve(self):
        account = Account.objects.create(owner=self.user, name='Test', currency=self.currency)
        assert Account.objects.get(pk=account.pk).pk == account.pk

        response = self.client.get('/v1/accounts/%s/' % account.pk)

        assert response.status_code == 200
        assert response.data['owner']['id'] == self.user.id
        assert response.data['owner']['username'] == 'test'
        assert response.data['name'] == 'Test'

    def test_account_forbid_post(self):
        account = Account.objects.create(owner=self.user, name='Test', currency=self.currency)
        assert Account.objects.get(pk=account.pk).pk == account.pk

        response = self.client.post('/v1/accounts/', {
            'owner': None,  # TODO: form a user
            'name': 'trololo',
        })

        assert response.status_code == 405

    def test_accounts_retrieve(self):
        account1 = Account.objects.create(owner=self.user, name='Test1', currency=self.currency)
        account1.register_incoming(Decimal('10'))

        user2 = User.objects.create_user(username='test2', password='12345')

        account2 = Account.objects.create(owner=user2, name='Test2', currency=self.currency)
        account2.register_incoming(Decimal('10'))
        account2.register_outgoing(Decimal('-9'))

        response = self.client.get('/v1/accounts/')
        assert len(response.data) == 1  # TODO: make better by adding further comparation

    def tearDown(self):
        pass
