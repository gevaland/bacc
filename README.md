** Description **

For the test, we ask that you create a simple account/transactions model with a RESTful API.  This should allow for updating the balances of accounts and handling the following endpoint:

- API endpoint ``GET /v1/accounts`` which shows a list of accounts
- API endpoint ``GET /v1/payments`` which shows a list of payments
- API ``POST /v1/payments`` that creates a new payment with given ``from_account``, ``amount``, ``to_account``.

** Run tests and check coverage **
```
python run_tests.py
```

** Run code style checks with pylint **
```
python run_pylint.py
```

** Run project locally **
```
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```
