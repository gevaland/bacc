"""" This module contains routing for users app. """

from django.conf.urls import url

from .views import UserRegistrationApiView, UserLoginApiView, UserLogoutApiView

urlpatterns = [
    url(r'^$', UserRegistrationApiView.as_view(), name="list"),
    url(r'^login/$', UserLoginApiView.as_view(), name="login"),
    url(r'^logout/$', UserLogoutApiView.as_view(), name="logout"),
]
