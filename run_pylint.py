#!/usr/bin/env python

from pylint.lint import Run
Run(['--load-plugins', 'pylint_django', 'accounts', 'users', 'payments'])
