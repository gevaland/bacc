"""" This module contains admin repr for Account. """

from django.contrib import admin

from .models import Account, Currency


class AccountAdmin(admin.ModelAdmin):
    """" Util class that helps to represent Account. """

    list_display = ('owner', 'name')
    list_filter = ('name',)


class CurrencyAdmin(admin.ModelAdmin):
    """" Util class that helps to represent Currency. """
    pass

admin.site.register(Account, AccountAdmin)
admin.site.register(Currency, CurrencyAdmin)
