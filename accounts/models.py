"""" This module contains models to work with bank user accounts. """

from __future__ import unicode_literals

from decimal import Decimal

from django.db import models, IntegrityError
from django.contrib.auth.models import User
from django.utils.encoding import smart_text as smart_unicode
from django.utils.translation import ugettext_lazy as _

from payments.models import Payment
from .exceptions import AccountNotEnoughResources, AccountLocked


class Locked(object):
    """" Used when you need to perform operation.
         Classic locks doesn't allow you to load balancing
         with many django app instances and consistency of transactions. """

    def __init__(self, account):
        self.account = account
        self.lock = None
        super(Locked, self).__init__()

    def __enter__(self):
        try:
            self.lock = Lock.objects.create(account=self.account)
        except IntegrityError:
            raise AccountLocked('Account %s already locked.' % self.account.pk)
        return self.lock

    def __exit__(self, exc_type, value, trace_back):
        self.lock.delete()


class Lock(models.Model):
    """ Model provides RDBMS-based locking for transactions in account. """

    account = models.OneToOneField('Account', unique=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return smart_unicode(self.account)

    class Meta:
        verbose_name = _('Lock')
        verbose_name_plural = _('Locks')


class Currency(models.Model):
    """ Model class for Account's Currency. """

    name = models.CharField(_('Currency Name'), max_length=255)
    code = models.CharField(_('Code'), max_length=3)

    def __unicode__(self):
        return smart_unicode(self.name)

    class Meta:
        verbose_name = _('Currency')
        verbose_name_plural = _('Currencies')


class Account(models.Model):
    """ Model class for Account. """

    owner = models.OneToOneField(User)
    name = models.CharField(_('Account Name'), max_length=255)
    currency = models.ForeignKey(Currency)

    def register_incoming(self, value, current_lock=None):
        if value <= Decimal('0'):
            raise ValueError('Value need to be positive, not %s.' % value)

        if current_lock is not None:
            if current_lock.account != self:
                raise ValueError('Lock for another account.')

            return self._create_transaction(value, Payment.DIRECTION_DICT[0][1])

        with Locked(self):
            return self._create_transaction(value, Payment.DIRECTION_DICT[0][1])

    def _create_transaction(self, value, direction):
        return Payment.objects.create(
            account=self,
            direction=direction,
            value=value)

    def register_outgoing(self, value, current_lock=None):
        if value >= Decimal('0'):
            raise ValueError('Value need to be negative, not %s.' % value)

        if current_lock is not None:
            if current_lock.account != self:
                raise ValueError('Lock for another account.')

            if self.balance >= value:
                return self._create_transaction(value, Payment.DIRECTION_DICT[0][0])
            else:
                raise AccountNotEnoughResources(
                    'N/A on balance to outgoing of %s.' % value)

        with Locked(self):
            if self.balance >= value:
                return self._create_transaction(value, Payment.DIRECTION_DICT[0][1])
            else:
                raise AccountNotEnoughResources(
                    'N/A on balance to outgoing of %s.' % value)

    @property
    def balance(self):
        value = self.payment_set.aggregate(models.Sum('value'))['value__sum']
        if value is None:
            value = Decimal(0)

        return value

    def __unicode__(self):
        return smart_unicode(self.name)

    class Meta:
        verbose_name = _('Account')
        verbose_name_plural = _('Accounts')
