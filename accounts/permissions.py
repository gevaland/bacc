"""" This module helps to protect access to Accounts. """

from rest_framework.permissions import BasePermission


class UserIsOwner(BasePermission):
    """" Checks is user own this Account obj. """

    def has_object_permission(self, request, view, obj):
        return request.user.id == obj.owner.id
