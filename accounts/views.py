""" This module consists of REST Framework APIs for Account. """

from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from .permissions import UserIsOwner
from .models import Account
from .serializers import AccountSerializer


class AccountListApiView(ListAPIView):
    """" REST API View that lists Accounts. """

    serializer_class = AccountSerializer

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user.id)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class AccountDetailApiView(RetrieveAPIView):
    """" REST API View that show detail of concrete Account. """

    serializer_class = AccountSerializer
    queryset = Account.objects.all()
    permission_classes = (IsAuthenticated, UserIsOwner)
