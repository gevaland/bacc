"""" This module contains routing for accounts app. """

from django.conf.urls import url

from .views import AccountListApiView, AccountDetailApiView

urlpatterns = [
    url(r'^$', AccountListApiView.as_view(), name="accounts-list"),
    url(r'^(?P<pk>[0-9]+)/$', AccountDetailApiView.as_view(), name="accounts-detail"),
]
