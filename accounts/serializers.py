""" This module contains serializers for Account manipulation. """

from rest_framework import serializers

from users.serializers import UserSerializer
from .models import Account, Currency


class CurrencySerializer(serializers.ModelSerializer):
    """ Implements Currency serializer. """

    class Meta:
        model = Currency
        fields = ('name', 'code')


class AccountSerializer(serializers.ModelSerializer):
    """ Implements User's Account serializer. """

    owner = UserSerializer(read_only=True)
    currency = CurrencySerializer(read_only=True)

    class Meta:
        model = Account
        fields = ('owner', 'name', 'currency')
