"""" This modules stores Account exceptions. """


class AccountLocked(Exception):
    """"  Throws when account is locked. """
    pass


class AccountNotEnoughResources(Exception):
    """" Throws when account has no resources. """
    pass
