"""" This module contains routing for accounts app. """

from django.conf.urls import url

from .views import PaymentListApiView, PaymentDetailApiView

urlpatterns = [
    url(r'^$', PaymentListApiView.as_view(), name="payments-list"),
    url(r'^(?P<pk>[0-9]+)/$', PaymentDetailApiView.as_view(), name="payments-detail"),
]
