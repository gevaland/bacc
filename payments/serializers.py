""" This module contains serializers for Account manipulation. """

from rest_framework import serializers

from accounts.serializers import AccountSerializer
from .models import Payment


class PaymentSerializer(serializers.ModelSerializer):
    """ Implements account payment serializer. """

    account = AccountSerializer(read_only=True)

    class Meta:
        model = Payment
        fields = ('account', 'value', 'direction')
