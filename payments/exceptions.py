"""" This modules stores Account exceptions. """


class PaymentTransactionException(Exception):
    """" Throws when transaction is broken. """
    pass


class IncorrectAmountException(Exception):
    """" Throws when incorrect amount specified in the payment. """
    pass
