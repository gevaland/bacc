"""" This module contains admin repr for Account. """

from django.contrib import admin

from .models import Payment


class PaymentAdmin(admin.ModelAdmin):
    """" Util class that helps to represent Currency. """
    pass

admin.site.register(Payment, PaymentAdmin)
