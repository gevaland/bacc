""" This module consists of REST Framework APIs for Account. """

import logging

from decimal import Decimal

from django.db import IntegrityError
from django.db import transaction
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated

from accounts.models import Account
from payments.exceptions import PaymentTransactionException, IncorrectAmountException

from .permissions import UserIsOwner
from .models import Payment
from .serializers import PaymentSerializer


class PaymentListApiView(ListCreateAPIView):
    """" REST API View that lists Payments. """

    serializer_class = PaymentSerializer

    def get_queryset(self):
        return Payment.objects.filter(account__owner=self.request.user.id)

    def post(self, request, *args, **kwargs):
        amount = self.request.data['amount']
        amount = Decimal(amount)

        if amount <= Decimal('0'):
            raise IncorrectAmountException('Amount %f is incorrect. Must be positive.' % amount)

        from_account_pk = self.request.data['from_account']
        to_account_pk = self.request.data['to_account']

        from_account = Account.objects.get(pk=from_account_pk)
        to_account = Account.objects.get(pk=to_account_pk)

        PaymentListApiView._register_transaction(from_account, to_account, amount)

        return super(PaymentListApiView, self).post(request, *args, **kwargs)

    @staticmethod
    def _register_transaction(from_account, to_account, amount):

        try:
            with transaction.atomic():
                from_account.register_outgoing(value=-amount)
                to_account.register_incoming(value=amount)
        except IntegrityError:
            msg = 'Transaction error with accounts PKs: %d and %d'\
                  % (from_account.pk, to_account.pk)
            logging.error(msg)
            raise PaymentTransactionException(msg)


class PaymentDetailApiView(RetrieveUpdateAPIView):
    """" REST API View that show detail of concrete Account. """

    serializer_class = PaymentSerializer
    queryset = Payment.objects.all()
    permission_classes = (IsAuthenticated, UserIsOwner)
