"""" This module contains models to work with account payments. """

from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Payment(models.Model):
    """ Model for Transactions on an Account. """

    DIRECTION_DICT = (
        ('OUT', 'Outgoing'),
        ('IN', 'Incoming'),
    )

    account = models.ForeignKey('accounts.Account')
    value = models.DecimalField(_('Amount value'), max_digits=10, decimal_places=4)
    direction = models.CharField(choices=DIRECTION_DICT, max_length=5)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s %s' % (self.account, '{0:.2f}'.format(self.value))

    class Meta:
        verbose_name = _('User payment')
        verbose_name_plural = _('User payments')
