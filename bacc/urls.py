from django.conf.urls import url, include
from django.contrib import admin


api_urls = [
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^payments/', include('payments.urls', namespace='payments')),
    url(r'^users/', include('users.urls', namespace='users')),
]

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^v1/', include(api_urls)),
]
