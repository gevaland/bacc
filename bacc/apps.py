from django.apps import AppConfig


class BaccConfig(AppConfig):
    name = 'bacc'
    verbose_name = 'A Much Better Name'
