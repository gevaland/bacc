from rest_framework.response import Response
from rest_framework.views import exception_handler

from payments import exceptions


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first, to get the standard error response.
    response = exception_handler(exc, context)

    if response is None:
        response = Response({}, status=400)

    response.data['status_code'] = response.status_code

    # Now add the HTTP status code to the response.
    if isinstance(exc, exceptions.IncorrectAmountException):
        response.data['detail'] = 'Incorrect amount query.'

    return response
